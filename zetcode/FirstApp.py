# -*- coding: utf-8 -*-
'''A basic trivial application'''

import sys
from  PySide import QtGui, QtCore


class MyWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        #Setting font
        font = QtGui.QFont('SansSerif',10)
        QtGui.QToolTip.setFont(font)
        #The tool tip for the widget
        self.setToolTip('This is a <b>QWidget</b> widget')
        #Manking a button
        btn = QtGui.QPushButton('Button',self)
        #Connect the event handler
        btn.clicked.connect(QtCore.QCoreApplication.instance().quit)
        btn.setToolTip('This is a <b>QPushButton</b> widget')
        btn.resize(btn.sizeHint())
        btn.move(50,50)
        #Setting the main window
        self.resize(250,150)
        self.center()
        self.setWindowTitle('First App')
        self.setWindowIcon(QtGui.QIcon('web.png'))
        self.show()

    def center(self):
        qr = self.frameGeometry() # A rectangle
        #Getting the center point
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    #Overriding a method
    def closeEvent(self, event):
        reply = QtGui.QMessageBox.question(self,'Message',\
                'Are you sure to quit?',\
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,\
                QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
