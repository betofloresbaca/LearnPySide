# -*- coding: utf-8 -*-
'''Grid Layout for widgets'''

import sys
from PySide import QtGui, QtCore


class MyWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        names = ['Cls', 'Bck', '', 'Close', '7', '8', '9', '/',
                '4', '5', '6', '*', '1', '2', '3', '-',
                '0', '.', '=', '+']
        grid = QtGui.QGridLayout()
        j=0
        for i in names:
            button = QtGui.QPushButton(i)
            if j == 2:
                grid.addWidget(QtGui.QLabel(''), 0, 2)
            else:
                grid.addWidget(button, j//4, j%4)
            j = j + 1
        self.setLayout(grid)
        self.move(300, 300)
        self.setWindowTitle('Grid Layout')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
        