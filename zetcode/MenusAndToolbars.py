# -*- coding: utf-8 -*-
'''Creatting menus and tool bars'''

import sys
from PySide import QtGui, QtCore


class MyWindow(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.initUI()

    def initUI(self):
        #Making the exit action
        exitAction = QtGui.QAction(QtGui.QIcon('exit.png'),\
                     '&Exit',self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit from the application')
        exitAction.triggered.connect(self.close)
        #The status bar
        self.statusBar().showMessage('Ready')
        #The menu bar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)
        #The toolbar
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAction)
        #The central widget
        textEdit = QtGui.QTextEdit()
        self.setCentralWidget(textEdit)
        #The position
        self.setGeometry(300,300,250,150)
        self.setWindowTitle('Menus and Toolbars')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()