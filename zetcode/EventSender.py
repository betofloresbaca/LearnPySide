# -*- coding: utf-8 -*-
'''Event sender'''

import sys
from PySide import QtGui, QtCore


class MyWindow(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.counter = 0
        self.initUI()

    def initUI(self):
        b1 = QtGui.QPushButton('-',self)
        b2 = QtGui.QPushButton('+',self)
        b1.move(10,40)
        b2.move(130,40)
        b1.clicked.connect(self.botonPresionado)
        b2.clicked.connect(self.botonPresionado)
        self.statusBar().showMessage("Count: 0")
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Event Sender')
        self.show()

    def botonPresionado(self):
        sender = self.sender()
        if sender.text() == '+':
            self.counter += 1
        else:
            self.counter -= 1
        msg = "Count: "+str(self.counter)
        self.statusBar().showMessage(msg)


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()