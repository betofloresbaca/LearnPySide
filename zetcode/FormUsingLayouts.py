# -*- coding: utf-8 -*-
'''A form window'''

import sys
from PySide import QtGui, QtCore


class FormWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        titleLabel = QtGui.QLabel('Title')
        autorLabel = QtGui.QLabel('Autor')
        reviewLabel = QtGui.QLabel('Review')
        titledit = QtGui.QLineEdit()
        autorEdit = QtGui.QLineEdit()
        reviewEdit = QtGui.QTextEdit()
        #The buttons and his horizontal layout
        okB = QtGui.QPushButton('Ok')
        cancel = QtGui.QPushButton('Cancel')
        hbl = QtGui.QHBoxLayout()
        hbl.addStretch(10)
        hbl.addWidget(cancel)
        hbl.addWidget(okB)
        #The grid layout
        grid = QtGui.QGridLayout()
        grid.setSpacing(10)
        grid.addWidget(titleLabel,0,0)
        grid.addWidget(titledit,0,1)
        grid.addWidget(autorLabel,1,0)
        grid.addWidget(autorEdit,1,1)
        grid.addWidget(reviewLabel,2,0)
        grid.addWidget(reviewEdit,2,1,5,1)
        grid.addLayout(hbl,7,0,1,2)
        self.setLayout(grid)
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Box Layout')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    w = FormWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
        