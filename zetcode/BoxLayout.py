# -*- coding: utf-8 -*-
'''Box Layout for widgets'''

import sys
from PySide import QtGui, QtCore


class MyWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        okB = QtGui.QPushButton('Ok')
        cancel = QtGui.QPushButton('Cancel')
        hbl = QtGui.QHBoxLayout()
        hbl.addStretch(10)
        hbl.addWidget(cancel)
        hbl.addWidget(okB)
        vbl = QtGui.QVBoxLayout()
        vbl.addStretch(10)
        vbl.addLayout(hbl)
        self.setLayout(vbl)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Box Layout')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
        