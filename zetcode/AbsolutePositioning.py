# -*- coding: utf-8 -*-
'''Absolute positioning of the widgets'''

import sys
from PySide import QtGui, QtCore


class MyWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        QtGui.QLabel('Luis', self).move(15, 10)
        QtGui.QLabel('Alberto', self).move(35, 40)
        QtGui.QLabel('Flores', self).move(55, 70)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Absolute Positioning')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
