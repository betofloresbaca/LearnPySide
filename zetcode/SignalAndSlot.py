# -*- coding: utf-8 -*-
'''Signal & Slot'''

import sys
from PySide import QtGui, QtCore


class MyWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        lcd = QtGui.QLCDNumber()
        slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        #Connecting
        slider.valueChanged.connect(lcd.display)
        #Setting the layout
        vbl = QtGui.QVBoxLayout()
        vbl.addWidget(lcd)
        vbl.addWidget(slider)
        self.setLayout(vbl)
        #The Geometry
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Box Layout')
        self.show()


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()