# -*- coding: utf-8 -*-
'''This is my own text editor made by using PySide'''

import sys
import PySide
from PySide import QtGui, QtCore


class TextEditor(QtGui.QMainWindow):
	
	def __init__(self):
		QtGui.QMainWindow.__init__(self)
		self.fileName = ""
		self.edited = False
		self.initUI()

	def initUI(self):
		#Setting the title
		self.setWindowTitle('My Own Editor')
		#Setting the icon
		self.setWindowIcon(QtGui.QIcon('logo.png'))
		#Setting font
		font = QtGui.QFont('SansSerif',10)
		QtGui.QToolTip.setFont(font)
		#The status bar
		self.statusBar().showMessage('Ready')
		#Exit Action
		exitAction = QtGui.QAction(QtGui.QIcon('../zetcode/exit.png'),\
					 '&Exit',self)
		exitAction.setShortcut('Ctrl+Q')
		exitAction.setStatusTip('Exit from the application')
		exitAction.triggered.connect(self.close)
		#OpenFile Action
		openFAction = QtGui.QAction(QtGui.QIcon('openFile.png'),\
					 '&Open File',self)
		openFAction.setShortcut('Ctrl+O')
		openFAction.setStatusTip('Open a file')
		openFAction.triggered.connect(self.openFile)
		#SaveFile Action
		saveFAction = QtGui.QAction(QtGui.QIcon('saveFile.png'),\
					 '&Save File',self)
		saveFAction.setShortcut('Ctrl+S')
		saveFAction.setStatusTip('Save the file')
		saveFAction.triggered.connect(self.saveFile)
		#The menu bar
		menubar = self.menuBar()
		fileMenu = menubar.addMenu('&File')
		fileMenu.addAction(openFAction)
		fileMenu.addAction(saveFAction)
		fileMenu.addAction(exitAction)
		#The central widget
		self.textEdit = QtGui.QTextEdit(self)
		self.setCentralWidget(self.textEdit)
		#Showing
		self.showMaximized()

	def openFile(self):
		pass

	def saveFile(self):
		pass

	#Overriding a method
	def closeEvent(self, event):
		reply = QtGui.QMessageBox.question(self, 'Message',\
				'Do you want to save the file: ' + self.fileName,\
				QtGui.QMessageBox.Yes | QtGui.QMessageBox.No |
				QtGui.QMessageBox.Cancel,\
				QtGui.QMessageBox.Cancel)
		if reply == QtGui.QMessageBox.Cancel:
			event.ignore()
		elif reply == QtGui.QMessageBox.No:
			event.accept()
		else:
			#TO DO
			pass


def main():
	app = QtGui.QApplication(sys.argv)
	te = TextEditor()
	sys.exit(app.exec_())


if __name__ == '__main__':
	main()
